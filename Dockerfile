############################################################
# Dockerfile to build NodeJS 4 Installed Containers
# Based on Node:12.22.12
############################################################

FROM node:12.22.12

ARG LISTEN_PORT=3000
ARG API_HOST=/

EXPOSE $LISTEN_PORT

# Install dependencies
RUN npm install -g forever

CMD [/bin/bash]
